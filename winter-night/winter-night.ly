\include "custom.ly"

\header {
	title = "Zimní noc"
	subtitle = "(A Winter Night)"
	subsubtitle = ""
	composer = "Darek Cidlinský"
	arranger = ""
	poet = ""
	meter = ""
	copyright = \markup\line{\lower #1.2 {\epsfile #Y #4 "../cc-by-sa.eps"} \italic{"Zveřejněno pod licencí / Licensed under:"} "Creative Commons Attribution-ShareAlike 4.0 International License." }
	tagline = ##f
}

segno-coda = \markup\left-align\magnify #1.2 \line{\musicglyph #"scripts.segno" \lower #.5 "—" \musicglyph #"scripts.coda"}

global = {
	\key e \minor
	\time 2/4
	\globalstyle
	\set Score.voltaSpannerDuration = #(ly:make-moment 3 1)
	\override Score.OttavaBracket.outside-staff-priority = #500
	\override TupletBracket.bracket-visibility = ##f
	\override TupletNumber.font-size = #-.5
}

lining = {
	\global

	s2*4 | \break
	s2*5 | \break
	s2*8 | \break
	s2*8 | \break
	s2*9 | \break
	s2*5 | \break
	\repeat unfold 3 { s2 | \noBreak }

}

melody = {	
	\global
	\voiceOne
	\tupletUp
	\slurDown

	\tempo\markup\main-tempo "Moderato molto tranquillo." 2 = 35


	\ottava #1 \osm
	\repeat volta 2 { s2 | }
	\transpose c c'
	{
		\segno
		\repeat volta 2 { e32 h' h' h' h h' h' h' h, h' h' h' h h' h' h'-\tweak extra-offset #'(1.5 . 8) _\markup\italic "Simile." } |
		\repeat volta 2 { s h'16.:32[ s32 h'16.:32 s32 h'16.:32 s32 h'16.:32] } |
		\repeat volta 2 { s32 a'16.:32[ s32 a'16.:32 s32 a'16.:32 s32 a'16.:32] } |
		\repeat volta 2 { s32 g'16.:32[ s32 g'16.:32 s32 g'16.:32 s32 g'16.:32] } |
		\repeat volta 2 { s32 fis'16.:32[ s32 fis'16.:32 s32 fis'16.:32 s32 fis'16.:32] } |
		\repeat volta 2 { s32 e'16.:32[ s32 e'16.:32 s32 e'16.:32 s32 e'16.:32] } |
		\coda
		\repeat volta 2 { s32 dis'16.:32[ s32 dis'16.:32 s32 dis'16.:32 s32 dis'16.:32] } |
		\repeat volta 2 { s32 e'16.:32[ s32 e'16.:32 s32 e'16.:32 s32 e'16.:32] } |
	}

	\ottava #0
	\repeat unfold 8 { \repeat percent 2 { s32 e'16.:32[ s32 e'16.:32 s32 e'16.:32 s32 e'16.:32] } | }
	\repeat percent 2 { s32 dis'16.:32[ s32 dis'16.:32 s32 dis'16.:32 s32 dis'16.:32] } |
	\repeat percent 2 { s32 d'?16.:32[ s32 d'16.:32 s32 d'16.:32 s32 d'16.:32] } |
	\repeat percent 2 { s32 h16.:32[ s32 h16.:32 s32 h16.:32 s32 h16.:32] } |
	s32 h16.:32[ s32 h16.:32 s32 h16.:32 s32 h16.:32]
	s32 e'16.:32[ s32 g'16.:32 s32 h'16.:32 s32 d''16.:32] |
	\flagFix <g' h' e''>2^\fermata_\segno-coda \bar "||"

	\coda-ending
	\ottava #1 \osm
	\transpose c c'
	{
		\repeat volta 2 { s32 e'16.:32[ s32 e'16.:32 s32 e'16.:32 s32 e'16.:32] } |
		s32 dis'16.:32[ s32 dis'16.:32 s32 dis'16.:32 s32 dis'16.:32] |
		s32 dis'16.:32[ s32 dis'16.:32 s32 dis'16.:32 s32 dis'16.:32] |
	}

	\set Timing.beamExceptions = #'()
	\set Timing.baseMoment = #(ly:make-moment 1/8)
	\set Timing.beatStructure = #'(1 1 1 1)
	\ottava #0
	<<
		{
			h32 s s s h s s s h s s s h s s s |
			h32 s s s h s s s c' s s s h s s s |
			c' s s s h s s s g s s s fis s s s |
			g s s s fis s s s dis s s s c s s s |
			dis s s s fis s s s dis s s s c s s s |
		}
		{
			\repeat unfold 20 { s32 fis' h dis'' } 
		}
	>>

	<h h fis' dis''>2\strum^\fermata \bar "|."
}

basses = {
	\global
	\voiceTwo

	\transpose c c'
	{
		\repeat volta 2 { e8_\markup\circle{4}_\markup{\italic "sempre" \dynamic "mp" \italic "dolce" } h_\markup\circle 3 <h,-0> h | }
		\repeat volta 2 { e8_\markup\circle 4 h_\markup\circle 3 <h,-0> h | }
		\repeat volta 2 { dis a h, a | }
		\repeat volta 2 { dis a h, a | }
		\repeat volta 2 { e h h, h | }
		\repeat volta 2 { d8 fis h, fis | }
		\repeat volta 2 { c8 g h, g | }
		\repeat volta 2 { h, fis h, fis | }
		\repeat volta 2 { \barre "½XII" { e g h g } | }
	}

	\tempo\markup\bold "Poco più agitato."
	\repeat percent 2 { h8\strum-\tweak extra-offset #'(0.25 . 4) _\markup\circle{4}_\markup\dynamic "mf" e'-\tweak extra-offset #'(.5 . 5)_\markup\circle{3} g'-\tweak extra-offset #'(0 . 5)_\markup\circle{2} e' | }
	\repeat percent 2 { a8\strum dis' fis' dis' | }
	\repeat percent 2 { \barre "½IV" { dis8\strum\<-\tweak extra-offset #'(0 . 3) _\markup\circle{5} fis-\tweak extra-offset #'(.75 . 8) _\markup\circle{4} h-\tweak extra-offset #'(.25 . 6)_\markup\circle{3} dis'-\tweak extra-offset #'(0 . 7) _\markup\circle{2} } | }
	\repeat percent 2 { g8\strum h e' h | }
	\repeat percent 2 { fis8\strum h d' h | }
	\tempo\markup\bold "Sempre più agitato."
	\repeat percent 2 { b,8\strum e g cis' | }
	\override Staff.Arpeggio.positions = #'(-5.5 . -3)
	\repeat percent 2 { a,8\strum dis a h | }
	\repeat percent 2 { h,8\strum\f e g h | }
	\repeat percent 2 { c8\strum fis a fis | }
	\repeat percent 2 { h,8\strum f a f | }
	\repeat percent 2 { a,\strum dis a dis | }
	\revert Staff.Arpeggio.positions
	e,_\markup{\italic "subito" \dynamic "mp"} h, e g |
	h h h h |
	\flag e2_\fermata 

	%Coda

	\tempo\markup\bold "Rall."
	\repeat volta 2 { cis'8_\markup\italic "molto dim." g' h g' | }
	h fis' h fis' |
	\tempo\markup\bold "A tempo."
	c' fis' h fis' |
	h[_\markup{\dynamic "pp" \italic "leggiero"} h] h[ h] |
	h[ h] c'[ h] |
	c'[ h] g[ fis] |
	\tempo\markup\bold "Rall. sempre al Fine."
	g[ fis] dis[ c] |
	dis[ fis] dis[ c] |
	h,2\strum_\fermata \bar "|."


}

ostinato = {
	\global
	\voiceFour

	\repeat unfold 9 { \repeat volta 2 { s2 | } }
	\repeat unfold 6 { e,2\strum~ | 2 | }
	fis,2\strum~ | 2 | 
	\repeat unfold 3 { e,2\strum~ | 2 | }
	fis,2\strum~ | 2 | 
}

\score {
	\new Staff = "up" \with {
		\consists "Span_arpeggio_engraver"
		instrumentName = \markup\center-column{"Kytara" \teeny "Guitar" }
	}
	{
		\set Staff.connectArpeggios = ##t
		<<
			\new Voice { \clef "treble_8" \melody }
			\new Voice { \clef "treble_8" \basses }
			\new Voice { \clef "treble_8" \ostinato }
			\new Voice { \clef "treble_8" \lining }
		>>
	}
	\layout { \context { \Staff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t } }
}

\score {
	\new Staff \with {
		midiInstrument = "acoustic guitar (steel)"
		midiMinimumVolume = 1
		midiMaximumVolume = 1
	}
	{
		<<
			\new Voice { \clef "treble_8" \unfoldRepeats \melody }
			\new Voice { \clef "treble_8" \unfoldRepeats \basses }
			\new Voice { \clef "treble_8" \unfoldRepeats \ostinato }
		>>
	}
	\midi {
		\context {
			\Score
		}
	}
}

\paper
{
}
