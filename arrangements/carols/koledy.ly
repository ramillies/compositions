\include "custom.ly"

\header {
	title = \markup{\sans \fontsize #7.5 "TŘI KOLEDY"}
	composer = "Pro kytaru upravil Darek Cidlinský"
	tagline = ##f
}

segno-coda = \markup\left-align\magnify #1.2 \line{\musicglyph #"scripts.segno" \lower #.5 "—" \musicglyph #"scripts.coda"}

tupletbracket = \once\override TupletBracket.bracket-visibility = ##t

kristusPanGlobal = {
	\key a \major
	\time 4/4
	\globalstyle
	\set Score.voltaSpannerDuration = #(ly:make-moment 3 1)
	\override Score.OttavaBracket.outside-staff-priority = #500
	\override TupletBracket.bracket-visibility = #'if-no-beam
	\override TupletNumber.font-size = #-.5
}

kristusPanLining = {
	\kristusPanGlobal
	\repeat unfold 5 { s1 | \noBreak } s | \break
	\repeat unfold 8 { s1 | \noBreak } s | \break
	\repeat unfold 7 { s1 | \noBreak } s | \break
	\repeat unfold 4 { s1 | \noBreak } s | \break

}

kristusPanMelody = {	
	\kristusPanGlobal
	\voiceOne
	\tupletUp

	\tempo\markup\main-tempo "Maestoso." \tempo 4 = 90

	a4\strum a8\strum( h) cis'4\strum dis'\strum |
	e'4 4 2 |
	dis'4 e' fis'2 |
	<gis h e'>1 |
	<a cis' e'>4 4 <fis a d'>4 4 |
	<e a cis'>4 q h2 |
	<e a cis'>4\strum <d a d'>\strum <e a cis'>\strum <fis gis h>\strum |
	<e a>1 |

	\repeat unfold 2 {
		<e a cis'>4 <e gis h> <e a cis'> <d a d'> |
		<gis h e'> 2 a |
	}
	a2 <h, a d' fis'> |
	<a cis' e'>4 <fis a d'> <e a cis'> <fis gis h> |
	a1 \bar "||"

	a4\strum a8\strum( h) cis'4\strum dis'\strum |
	e'2 2 |
	dis'4 e' fis'2 |
	<gis h>1 |

	\transpose c c'
	{
		<e gis e'>4 4 <h, fis d'>4 4 |
		<e a cis'>4 4 <fis gis h>2 |
		<e gis cis'>4 <h fis d'>4 <e gis cis'>4 <fis gis h>4 |
	}
	<a h cis' a'>1 |

	<e a cis'>4 <e gis h> <e a cis'> <d a d'> |
	<gis h e'>2 <e a> |
	<e a cis'>4 <e gis h> <e a cis'> <d a d'> |
	<gis h e'>2 <e a>4 8 <e gis> |
	<cis e a>2 <a, d a d' fis'> |
	<a cis' e'>4 <fis a d'>8 <e a cis'> <a cis' eis'> <b d' fis'> <h dis' fisis'> <c' e' gis'> |
	<a cis' a'>1^\fermata\strum |

	a,2 <fis, d> |
	<cis e>4 <h, d> cis4^\fermata h,^\fermata |
	a,1^\fermata \bar "|."


}

kristusPanBasses = {
	\kristusPanGlobal
	\voiceFour

	s1*3 |
	e4 4 d? d |
	s1 |
	cis8 d cis a, s2 |
	s1*5 |
	s2 e2 |
	<cis e>2 s |
	s1*5 |
	<e e'>4 4 <d d'>4 4 |

	cis'4 8 d' d'4 d'8 e' |
	cis'8 d' cis'4 s2 |
	cis'4 d' cis' s |
	s1*7 |
	\once\override Rest.extra-offset = #'(2.3 . 4) \once\override Dots.extra-offset = #'(2.3 . 4) r2. a,8 gis, |
	a,8 gis, fis, e, h, cis d e |
	a,4 fis, fis,_\fermata gis,_\fermata |
	a,1_\fermata \bar "|."
}

kristusPanOstinato = {
	\kristusPanGlobal
	\voiceTwo

	<a, e>4\strum 4\strum <a, e a>\strum <h, fis h>\strum |
	<e, gis h>4 4 2 |
	<h, fis h>4 <e, gis h> <h, h dis'>2 |
	e,1 |
	a,4 a,8( h,) h, cis d4 |
	fis,2 <e, h, fis gis> |
	a,4\strum a,\strum a,\strum h,\strum |
	a,1 |

	a,4 gis, a, fis, |
	cis2 <a, e> |
	a,4 gis, a, fis, |
	cis2 a,4 gis, |
	fis,4 e, fis, d |
	cis4 h, fis, e, |
	<a, e>1 \bar "||"

	<a, e>4\strum 4\strum <a, e a>\strum <h, fis h>\strum |
	<e, gis h>2 2 |
	<h, fis h>4 <e, gis h> <h, h dis'>2 |
	e,1 |
	a,2 2 |
	fis2 e |
	a,2. e4 |
	a,1 |

	a,8 e, h, e, a, e, fis, h, |
	cis8 h, a, gis, a, e, fis, e, |
	a,8 e, h, e, a, e, fis, h, |
	cis8 h, a, gis, a, e, a,->[ gis,->] |
	fis,8 gis, a, cis fis, h, d e |
	<fis, cis e>4 <e, h,>8 a, b, a, gis, g, |
	<fis, cis e>1\strum_\fermata |

}

kristusPanDynamika = {
	\kristusPanGlobal
	s1*4_\markup{\dynamic "f" \italic "sempre arpeggiando"} |
	s1*4_\mp |
	s1*3_\mf s1\< |
	s1 s1\> s1\! |

	s1*4_\f
	s1*4_\markup{\dynamic "p" \italic "dolce" } |
	s1*2_\mp s1_\textCresc "cresc." | s2 s_\textCresc "molto al" |
	s1\f | s2 s\< | s2\ff s4 s_\markup{\dynamic "mp" \italic "e dim. poco a poco"}
}

kristusPanSlova = \lyricmode {
	\override LyricText.font-shape = #'italic
	\set stanza = "1. "
	Na -- ro -- dil se Kris -- tus Pán, ve -- sel -- me se! __ Z~rů -- že kví -- tek vy -- kvet nám, ra -- duj -- me __ _ se. __ Z~ži -- vo -- ta čis -- té -- ho, z~ro -- du krá -- lov -- ské -- ho, nám, nám, na -- ro -- dil __ _ se. __ 

	\set stanza = "2. "
	Člo -- vě -- čen -- ství na -- še, ve -- sel -- me se, __ rá -- čil vzí -- ti na __ _ se, ra -- duj -- me __ _ se. __ Z~ži -- vo -- ta čis -- té -- ho, z~ro -- du krá -- lov -- ské -- ho, __ _ _ nám, nám, na -- ro -- _ dil __ _ _ _ se, __ nám, nám, na -- ro -- dil __ _ se. __

}

tichaNocGlobal = {
	\key g \major
	\time 3/4
	\globalstyle
	\set Score.voltaSpannerDuration = #(ly:make-moment 3 1)
	\override Score.OttavaBracket.outside-staff-priority = #500
	\override TupletBracket.bracket-visibility = #'if-no-beam
	\override TupletNumber.font-size = #-.5
}

tichaNocLining = {
	\tichaNocGlobal

	\repeat unfold 4 { s2. | \noBreak } \time 13/4 s4*13 | \break
	\time 3/4

	\repeat unfold 7 { s2. | \noBreak } s | \break
	\repeat unfold 7 { s2. | \noBreak } s | \break
	\repeat unfold 7 { s2. | \noBreak } s | \break

	\repeat unfold 3 { s2. | \noBreak } s | \break
	\repeat unfold 3 { s2. | \noBreak } s | \break
	\repeat unfold 3 { s2. | \noBreak } s | \break
	\repeat unfold 3 { s2. | \noBreak } s | \break

	\repeat unfold 5 { s2. | \noBreak } s | \break

}

tichaNocMelody = {	
	\tichaNocGlobal
	\voiceOne
	\tupletUp

	\tempo\markup\main-tempo "Andante." \tempo 4 = 75

	\transpose c c'
	{
		<c d e g>2\strum 4\strum |
		<e e a>2\strum 4\strum |
		<d e>2\strum 4\strum |
		<b, c f>2\strum 4\strum |
		\tempo\markup\italic "Quasi Cadenza."
		\once\omit Staff.TimeSignature \time 13/4 <fis h>2.^\fermata\strum fis8( e) dis( cis) ais,( gis,) fis,2 e,8( fis,) g,( a,) h,( c) \times 2/3 { d4 e d } \bar "||"

		\once\omit Staff.TimeSignature \time 3/4

		\tweak extra-offset #'(0 . -5) \a-tempo
		\repeat unfold 2 { d4. e8 d4 | <g, h,>2. | }
		<a, d a>2 4 |
		<d, a, c fis>2 \times 2/3 { fis8( e fis) } |
		<g, h, g>2 4 |
		<fis, h, d>2. |

		\repeat unfold 2 {
			e2\strum <h, d e>4 |
			g4. fis8 e4 |
			d4. e8 d4 |
			\priraz{d8} h,2. |
		}

		a4 4 4 |
		<fis c'>4 <a, a> <fis, fis> |
		<g, d g\laissezVibrer>4 s \times 2/3 { g8 8 8 } |
		\grace{g8\glissando} h2.^\fermata |
		g4 d h, |
		d4 c a, |
		<e, a, c>2.\strum |
		\tweak extra-offset #'(0 . -5) \rall <es, as, c>2. \bar "||"

		\tempo\markup\main-tempo "Più mosso."
		\repeat unfold 2 { d4~ \times 2/3 { d4 e8 } d4 | h,2. }
		a4 <e a> q |
		\omit TupletBracket \omit TupletNumber
		fis2 \times 2/3 { fis8( e) fis } |
		g4 4 \times 2/3 { g8 h, g, } |
		d2. |

		\repeat unfold 2 {
			e2\strum e4\strum |
			g4 \times 2/3 { d,8 <d g> <b, fis> } <b, e>4 |
			d4 \once\undo\omit TupletBracket \once\undo\omit TupletNumber \times 2/3 { d4 e8 } \priraz{d16 e} d4 |
			\priraz{d8} h,2. |
		}

		<a, c e a>4 <c e a> <h, c e a> |
		<a, dis fis c'>4 <fis, c dis a> <dis, a, c fis> |
		<g, d g>2.\strum |
		\grace{g16\glissando} h2.^\fermata |
		g4 d h, |
		d4 c \times 2/3 { e8 fis g } \bar "||"

		\tupletSpan 4 \times 2/3 { r8 <h, e> <e, d> <h, e> <e, d> <h, e> h, <h, e> <e, d> } |
		\times 2/3 { r8 <g, e> <fis, d> <g, e> <fis, d> <g, e> d e <g, d> } |
		\times 2/3 { r8 <a, e> <f, d> <a, e> <f, d> <a, e> d <a, e> <f, d> } |
		\times 2/3 { r8 <g, f> <es, d> <g, f> <es, d> <g, f> \tempo "Rall. a poco" g <d, g, d g> q } |

		\slurDown
		\a-tempo \priraz{fis16} a4~ \times 2/3 { a4 8 a fis a } |
		\times 2/3 { h8(\strum e4) } fis4 \times 2/3 { fis8( e) e } |
		\priraz{fis16} a2 fis4 |
		\slurUp
		h4\strum 4 \once\undo\omit TupletBracket \once\undo\omit TupletNumber
		\once\override TupletBracket.extra-offset = #'(0 . 1)
		\once\override TupletNumber.extra-offset = #'(0 . 1)
		\times 2/3 { <cis fis>8[( <h, e>]) <h, e>\strum } |
		\priraz{fis16} a2. |
		fis2 \times 2/3 { g8( e g) } |
		<gis, cis fis a~>4 \times 2/3 { a4 <cis e h>8 } <gis, cis fis a>4 |
		\tupletSpan 2
		\once\undo\omit TupletBracket \once\undo\omit TupletNumber \times 4/6 { fis8[ e fis a h cis'] } e'4^\fermata\strum \bar "|."
	}

}

tichaNocBasses = {
	\tichaNocGlobal
	\voiceFour

	e2\strum 4\strum |
	<fis d'>2\strum 4\strum |
	<h, g a>2\strum 4\strum |
	d2\strum 4\strum |
	\time 13/4 s4*13 \bar "||"

	\time 3/4
	\repeat unfold 2 {
		<fis g>2. |
		d4. e8 d4 |
	}

	d4 4 4 |
	s2.*3 |

	\repeat unfold 2 {
		<e a d'>2\strum s4 |
		<d c' d'>2 <d b>4 |
		<fis g>2 4 |
		s4 << { \voiceThree h8 g e'4 } \\ { \voiceFour e4 4 } >> |
	}

	<a c' e'>4 4 <h c' e'> |
	<a dis' fis'>4 s2 |
	s4 <e g d'>8 h, q4 |
	h4\rest <a cis' g'>2\strum_\fermata |
	s2.*4 \bar "||"

	\once\override TupletBracket.extra-offset = #'(1.1 . -0.5)
	\once\override TupletNumber.extra-offset = #'(1.1 . -0.5)
	\tupletSpan 4 \times 2/3 { \tweak extra-offset #'(1.1 . 0) c8\rest fis g fis g fis g fis d } |
	\omit TupletBracket \omit TupletNumber
	\override Rest.extra-offset = #'(0.9 . 0)
	\times 2/3 { d,8\rest g, d h g <e g> <d g> d h, } |
	\times 2/3 { c8\rest fis g fis g fis g fis d } |
	\times 2/3 { d,8\rest g, d h g <e g> <d g> d h, } |

	\times 2/3 { a,8\rest d, d e' c' fis e' c' fis } |
	\times 2/3 { g,,8\rest a, d c' a fis' } <d a c'>4 |
	\times 2/3 { a,,8\rest e g h g e } <g h g'>4
	<< { \voiceThree \omit TupletBracket \omit TupletNumber \times 2/3 { \tweak extra-offset #'(2.5 . 0) d'4\rest <a d'>8 <a d'>8 fis <d' e'> <a d'> fis <d' e'> } } \\ { \once\override NoteColumn.force-hshift = #1 \voiceFour \times 2/3 { a,8 h, a, } h,2 } >> |

	\override Rest.extra-offset = #'(1.9 . 0)
	\repeat unfold 2 {
		\times 2/3 { g,8\rest c e <d' e'> h d f,8\rest h, e } |
		<d c' d'>2. |
		\override Rest.extra-offset = #'(0.9 . 0)
		\times 2/3 { d8\rest fis g d' g fis d\rest fis g } |
		\times 2/3 { h,8\rest h, e g h d' e' fis' e' } |
	}
	s2.*2 |
	\times 2/3 { a,,8\rest <e, e> <h, g> \stemUp <e d'> <g g'> <e d'> <g g'> <e d'> <g g'> \stemDown } |
	s2. |
	\times 2/3 { d,8\rest g fis d' g fis h fis d } |
	\times 2/3 { a,,8\rest a, d c' d a, } s4 |
	s2.*4 |
	\revert Rest.extra-offset

	<<
		{
			\voiceThree
			\omit TupletBracket \omit TupletNumber \tupletSpan 4
			
			\times 2/3 { \tweak extra-offset #'(2 . 0) fis4\rest d8 \once\override Beam.positions = #'(2.5 . 1.5) cis' h a h4 a8 } |
			\times 2/3 { \tweak extra-offset #'(2 . 0) fis4\rest h8 fis fis g } s4 |
			\times 2/3 { \tweak extra-offset #'(2 . 0) fis4\rest d8 cis' h a g\rest h d } |
			\times 2/3 { \tweak extra-offset #'(1.5 . 0) h8\rest fis cis' cis' fis' d } s4 |
			\times 2/3 { \tweak extra-offset #'(2 . 0) fis4\rest d8 a cis' h a cis' a } |
		}
		\\
		{
			\voiceFour \tupletSpan 4
			\override Glissando.bound-details.right.X = 37.5
			\override Glissando.bound-details.right.Y = -4.5
			\times 2/3 { \tweak extra-offset #'(0.5 . 0) g,,8\rest a,4~ 4 h,8 a,4 fis,8\glissando } |\noBreak
			<fis g h>4\strum <fis g>4 4 |
			\omit TupletBracket \omit TupletNumber \tupletSpan 4
			\times 2/3 { \tweak extra-offset #'(0.5 . 0) g,,8\rest a,4~ 4 h,8 a,4 fis,8 } |
			<fis h cis'>4\strum <fis fis'> \once\undo\omit TupletBracket \once\undo\omit TupletNumber \times 2/3 { <d g>4 <e fis g>8\strum } |
			\times 2/3 { \tweak extra-offset #'(0.5 . 0) g,,8\rest a,4~ 4 h,8 } a,4 |
		}
	>>
	\times 2/3 { cis'8\rest a h cis' e' fis' } s4 |
	\times 2/3 { h,8\rest gis cis' fis' cis' gis  d8\rest cis' gis } |
}

tichaNocOstinato = {
	\tichaNocGlobal
	\voiceTwo

	s2. |
	d,2.~ |
	d,~ |
	d, |
	\time 13/4 <dis h cis'>2.\strum s4*10 \bar "||"

	\time 3/4
	\repeat unfold 4 { g,4 4 4 | }
	<d, a,>2. |
	a,4 a, <d a c'> |
	e,4 e e, |
	d,4 4 4 |
	
	\repeat unfold 2 {
		c4\strum c h, |
		a,4 d,2 |
		g,4 g, fis, |
		<e, e g>2. |
	}

	a,4 a, a, |
	a,2.\strum |
	<e, h, e>2. |
	a,2._\fermata |
	<g, fis g>4 4 4 |
	<d, d a>4 4 <d, a, d>4 |
	g,4\strum 4 4 |
	g,4 4 4 \bar "||"

	\repeat unfold 2 { <g, fis g>2. | <g, d g> | }
	<d, d fis c' e'>2.\strum |
	<d, a, d a c'>2. |
	<e, h, e g h>2. |
	<d, fis a>2. |
	\omit TupletBracket \omit TupletNumber

	\repeat unfold 2 {
		<c e h d'>2\strum  <h, e h d'>4\strum |
		\times 2/3 { a,8 d, a, } d4 \times 2/3 { b8 d a, } |
		<fis, fis g>2 4 |
		<e, e g>2. |
	}

	\times 2/3 { a,8 d, d } \times 2/3 { c' d a, } h4 |
	<d, a,>2. |
	<e, h, e>2.\strum |

	\set tieWaitForNote = ##t
	a,16~ a~ cis'~ g'~ <a, a cis' g'>2_\fermata |
	<g, d fis g h>2. |
	<d, a, d a>2 <d a c'>4 \bar "||"

	<c e h d'>2 <c h>4 |
	<h, fis g d'>2 <h, fis g>4 |
	<b, f a d'>2 <d, b, f>4 |
	<es, b, es g d'>2 <es, b, d g d'>4 |

	<d, a, d a cis'>2.\strum |
	g,2.\strum |
	<d, a, d a cis'>2.\strum |
	g,2.\strum |
	<d, a, d a cis'>2.\strum |
	<fis, fis>2 <e, h, fis g d'>4 |
	d,2. |
	d,2 <d, fis a e' gis'>4\strum_\fermata \bar "|."
}

tichaNocSlova = \lyricmode {
	\override LyricText.font-shape = #'italic
	\repeat unfold 19 { \skip 4 }
	\set stanza = "1. "
	Ti -- _ chá noc, sva -- _ tá noc, ja -- la lid __ _ v~bla -- hý klid,
	dvé jen srd -- cí tu v~Be -- tlé -- mě bdí, hvě -- zdy při svi -- tu u jes -- lí dlí, __
	v~nichž ma -- lé dě -- ťát -- ko spí, __ _ _ _ _ v~nichž ma -- lé dě -- ťát -- ko spí. __ _

	\set stanza = "2. "
	Ti -- _ chá noc, sva -- _ tá noc! Co __ _ an -- děl __ _ _ vy -- _ prá -- _ _ věl, __
	při -- šed s~ja -- _ _ snos -- tí v~pas -- _ tý -- řův stan, __
	zní již s~vý -- _ _ sos -- ti, s~všech __ _ ze -- mě stran: __
	„Vám je dnes spa -- si -- tel dán, __ _ při -- _ šel Kris -- _ tus __ _ _ Pán!“...

}

tichaNocDynamika = {
	\tichaNocGlobal

	s2.*4_\p
	\time 13/4 s4*13 |
	s2.*8_\mf |
	s2.*2\< s\> |
	s2.*2\< s\> |
	s2.*2\!_\markup\italic "cresc." |
	s2.\f |
	s4 s s-\tweak extra-offset #'(-2 . 2) _\markup\italic "dim." |
	s2.*4_\mp |

	s2.*16_\mf |
	s2._\textCresc "cresc. al" | s2.*2_\f | s16 s\!_\markup{\italic "dim. molto"} s s s2 |
	s2.*2_\mp |
	s2. | s2._\textCresc "cresc." | s_\textCresc "molto" s\textCresc "al" |
	s\!_\markup{\dynamic "f" \italic "e marcato il basso"} |
	s-\tweak extra-offset #'(0 . 4) _\markup\italic "calando poco a poco sin al Fine" |
	s2.*4 | s2._\markup{\dynamic "p" \italic "misterioso" } |
}

stestiGlobal = {
	\key a \major
	\time 4/4
	\globalstyle
	\set Score.voltaSpannerDuration = #(ly:make-moment 3 1)
	\override Score.OttavaBracket.outside-staff-priority = #500
	\override TupletBracket.bracket-visibility = #'if-no-beam
	\override TupletNumber.font-size = #-.5
}

stestiLining = {
	\stestiGlobal

}

stestiMelody = {	
	\stestiGlobal
	\voiceOne
	\tupletUp

	\tempo\markup\main-tempo "Andantino." \tempo 4 = 70

	<gis h cis' e'>2.\strum <h cis' fis'>4 |
	<gis h cis' e'>2.\strum <h cis' fis'>4 |
	\repeat unfold 2 {
		<gis cis' e'>4 <a cis' fis'> <gis cis' e'> <gis cis'> |
		<gis cis' e'>4 <gis cis'> <fis gis h> <e a> | |
	}

	\repeat unfold 2 { <e gis cis'>8 <e gis h> <e gis cis'> <d a d'> <gis h e'>4 <e a> | }
	<gis cis' e'>4 <a cis' fis'> <h e' e'> <e h cis'> |
	<h, fis a d' e'>4\strum <h, fis a d'>8 <h, fis a cis'> h4.\strum a8 |
	<e a>4 <e gis>4 4 <h cis' fis'> |
	<gis h cis' e'>2.\strum <h cis' fis'>4 |
	<gis h cis' e'>1\strum \bar "||"

	\transpose c c' {
		\repeat unfold 2 {
			<cis e gis e'>4 <a cis' fis'> <cis e gis e'> <cis e gis cis'> |
			<cis e gis e'> <cis e gis cis'> <fis gis h> <h, cis a> |
		}
	}

	\repeat unfold 2 { <e gis cis'>8 <e gis h> <e gis cis'> <d a d'> <gis h e'>4 <e a> | }
	<gis cis' e'>4 <a cis' fis'> <h e' e'> <e h cis'> |
	<h, fis a d' e'>4\strum <fis a d'>8 <fis a cis'> h4.\strum a8 |
	<e a>4 <e gis>4 4 <h cis' fis'> |
	<gis h cis' e'>2.\strum <h cis' fis'>4 |
	<gis h cis' e'>2.\strum <h cis' fis'>4 |
	<gis h cis' gis'>2.\strum <h cis' a'>4 |

	\flagOn s4 e'''2.^\fermata \bar "|."
}

stestiBasses = {
	\stestiGlobal
	\voiceFour

	r8 a, <gis h cis'> a, q a, d a, |
	r8 a, <gis h cis'> a, q a, d a, |
	s1*9 |
	r8 a, <gis h cis'> a, q a, d a, |
	r8 a, <gis h cis'> a, q a, q a, |
	s1*9 |
	r8 a, <gis h cis'> a, q a, d a, |
	r8 a, <gis h cis'> a, q a, d a, |
	r8 a, <gis h cis'> a, q a, d h, |

	\set tieWaitForNote = ##t
	\arpeggioBracket \shiftOff
	\flagOn <a, cis''>16\arpeggio~ gis'~ a'~ fis''~ <gis' a' cis'' fis''>2. \bar "|."
}

stestiOstinato = {
	\stestiGlobal
	\voiceTwo

	a,1\strum |
	a,1\strum |

	\repeat unfold 2 {
		e8 a, d a, e a, e a, |
		e a, e a, h, e, a, e, |
	}
	\repeat unfold 2 { a,8 gis, a, fis, cis e, a, e, | }
	a,8 e d h cis gis fis, <cis a> |
	e,2\strum <e, h, fis gis>\strum |
	a,8 4 8~ 8 8 d a, |
	a,1\strum |
	a,1\strum |

	\repeat unfold 2 {
		cis'8 a, d a, cis' a, cis' a, |
		cis' a, cis' a, e e, a a, |
	}

	\repeat unfold 2 { a,8 gis, a, fis, cis e, a, e, | }
	a,8 e d h cis gis fis, <cis a> |
	e,2\strum <e, h, fis gis>\strum |
	a,8 4 8~ 8 8 d a, |
	a,1\strum |
	a,1\strum |
	a,2.\strum s4 |
	a,4~ a,2._\fermata \bar "|."
}

stestiSlova = \lyricmode {
	\override LyricText.font-shape = #'italic
	\repeat unfold 4 { \skip 4 }
	\set stanza = "1. "
	Dej Bůh štěs -- tí to -- mu do -- mu; my zpí -- vá -- me, ví -- me ko -- mu: ma -- lé -- mu dě -- ťát -- ku, Kris -- tu Je -- zu -- lát -- ku, dnes v~Be -- tlé -- mě na -- ro -- ze -- né -- _ mu.

	\repeat unfold 6 { \skip 4 }

	\set stanza = "2. "
	On roz -- dá -- vá ště -- drov -- nič -- ky, jab -- ka, hruš -- ky i tro -- níč -- ky. Za na -- še zpí -- vá -- ní, za ko -- le -- do -- vá -- ní, dej nám Pán Bůh své po -- žeh -- ná -- _ ní.

}

stestiDynamika = {
	\stestiGlobal

	s1_\mp s s1*4_\markup\italic "marcata la melodia"
	s2\< s\> | s\< s\> |
	s1\mf | s2_\markup\italic "pieno voce" s_\markup\italic "dim." |
	s1*7\mp |
	s2\< s\> | s\< s\> |
	s1\mf | s1_\f | s1_\markup{\dynamic "mp" \italic "dim."}
}

\markup{ \fill-line { \center-align { \sans \italic \fontsize #6 {\bold "Narodil se Kristus Pán"} } } }


\score {
	\new PianoStaff \with {
		instrumentName = "Kytara"
		\remove "Keep_alive_together_engraver"
	}
	<<
		\new Lyrics = "slova-sopran" \with { \override VerticalAxisGroup #'staff-affinity = #DOWN } 
		\new Staff = "up" \with {
			\consists "Span_arpeggio_engraver"
		}
		{
			\set Staff.connectArpeggios = ##t
			<<
				\clef "G_8"
				\new Voice = "melody" { \removeWithTag #'midi \kristusPanMelody }
				\new Voice { \removeWithTag #'midi \kristusPanBasses }
				\new Voice { \removeWithTag #'midi \kristusPanOstinato }
				\new Voice { \kristusPanLining }
				\new Dynamics { \kristusPanDynamika }
			>>
		}
		\context Lyrics = "slova-sopran" \lyricsto "melody" \kristusPanSlova
	>>
	\layout
	{
		\context { \Staff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
		\context { \RhythmicStaff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
	}
}

\score {
	\new Staff \with {
		midiInstrument = "acoustic guitar (steel)"
		midiMinimumVolume = 1
		midiMaximumVolume = 1
	}
	{
		<<
			\new Voice { \unfoldRepeats \kristusPanMelody }
			\new Voice { \unfoldRepeats \kristusPanBasses }
			\new Voice { \unfoldRepeats \kristusPanOstinato }
		>>
	}
	\midi {
		\context {
			\Score
		}
	}
}

\markup{ \fill-line { \center-align { \sans \italic \fontsize #6 {\bold "Tichá noc"} } } }


\score {
	\new PianoStaff \with {
		instrumentName = "Kytara"
		\remove "Keep_alive_together_engraver"
	}
	<<
		\new Lyrics = "slova-sopran" \with { \override VerticalAxisGroup #'staff-affinity = #DOWN } 
		\new Staff = "up" \with {
			\consists "Span_arpeggio_engraver"
		}
		{
			\set Staff.connectArpeggios = ##t
			<<
				\clef "G_8"
				\new Voice = "melody" { \removeWithTag #'midi \tichaNocMelody }
				\new Voice { \removeWithTag #'midi \tichaNocBasses }
				\new Voice { \removeWithTag #'midi \tichaNocOstinato }
				\new Voice { \tichaNocLining }
				\new Dynamics { \tichaNocDynamika }
			>>
		}
		\context Lyrics = "slova-sopran" \lyricsto "melody" \tichaNocSlova
	>>
	\layout
	{
		\context { \Staff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
		\context { \RhythmicStaff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
	}
}

\score {
	\new Staff \with {
		midiInstrument = "acoustic guitar (steel)"
		midiMinimumVolume = 1
		midiMaximumVolume = 1
	}
	{
		<<
			\new Voice { \unfoldRepeats \tichaNocMelody }
			\new Voice { \unfoldRepeats \tichaNocBasses }
			\new Voice { \unfoldRepeats \tichaNocOstinato }
		>>
	}
	\midi {
		\context {
			\Score
		}
	}
}

\markup{ \fill-line { \center-align { \sans \italic \fontsize #6 {\bold "Dej Bůh štěstí"} } } }


\score {
	\new PianoStaff \with {
		instrumentName = "Kytara"
		\remove "Keep_alive_together_engraver"
	}
	<<
		\new Lyrics = "slova-sopran" \with { \override VerticalAxisGroup #'staff-affinity = #DOWN } 
		\new Staff = "up" \with {
			\consists "Span_arpeggio_engraver"
		}
		{
			\set Staff.connectArpeggios = ##t
			<<
				\clef "G_8"
				\new Voice = "melody" { \removeWithTag #'midi \stestiMelody }
				\new Voice { \removeWithTag #'midi \stestiBasses }
				\new Voice { \removeWithTag #'midi \stestiOstinato }
				\new Voice { \stestiLining }
				\new Dynamics { \stestiDynamika }
			>>
		}
		\context Lyrics = "slova-sopran" \lyricsto "melody" \stestiSlova
	>>
	\layout
	{
		\context { \Staff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
		\context { \RhythmicStaff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t }
	}
}

\score {
	\new Staff \with {
		midiInstrument = "acoustic guitar (steel)"
		midiMinimumVolume = 1
		midiMaximumVolume = 1
	}
	{
		<<
			\new Voice { \unfoldRepeats \stestiMelody }
			\new Voice { \unfoldRepeats \stestiBasses }
			\new Voice { \unfoldRepeats \stestiOstinato }
		>>
	}
	\midi {
		\context {
			\Score
		}
	}
}

\markup{\italic "Svorka: při flageoletu se dotknout struny jen letmo tak, aby zněl i základní tón"}

\paper
{
	page-count = #3
}
