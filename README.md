# Compositions

This repository contains scores, Lilypond files and recordings for some of my compositions.

## How to compile the scores

1. Get the `.ly` file for the score. Download the `custom.ly` file from the root of the repository as well and put it in the same directory.
2. Grab the contents of the `fonts` directory. The easiest way is to hit the button *left* to the blue *Clone* button (it has an arrow on it) and download everything as one ZIP.
3. Install the *ross* font like this: the directory has two subdirectories, `otf` and `svg`. On Linux, copy both of those into `/usr/share/lilypond/X.XX.X/fonts`, where `X.XX.X` is your Lilypond version. On Windows, there will presumably be a similar directory in wherever you have your Lilypond installed.
4. Install all the other fonts in the directory in the usual way (your system needs to recognize these fonts).
5. Now it should compile. Happy Lilyponding!
