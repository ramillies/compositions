\include "custom.ly"

\header {
	title = "Hodinový strojek"
	subtitle = "(The Clockwork)"
	subsubtitle = ""
	composer = "Darek Cidlinský"
	arranger = ""
	meter = ""
	tagline = ##f
}

segno-coda = \markup\left-align\magnify #1.2 \line{\musicglyph #"scripts.segno" \lower #.5 "—" \musicglyph #"scripts.coda"}

global = {
	\key e \minor
	\time 6/8
	\globalstyle
	\override TupletBracket.bracket-visibility = ##f
	\override TupletNumber.font-size = #-.5
}

lining = {
	\global

}

melody = {	
	\global
	\voiceOne
	\tupletUp
	\slurDown

	\tempo\markup\main-tempo "Andantino." 8 = 95

	\transpose c c'
	{
		e'4 fis'8 g' fis' e' |
		d' c'4~ 4. |
		d'4 e'8 d'4 c'8 |
		h4 e8 g4 fis8 |
		e4 h,8 c4 h,8 |
		a,4 h,8 gis,4. |
		<e, a,>8 8 8 a,4 h,8 |
		r4 \flagFixOn h,8\laissezVibrer <dis gis>4.^\fermata\laissezVibrer \flagFixOff \bar "||"

		\coda

		\flagFixOn \ottava #1 \arm-artif-osm
		\transpose c c'
		{
			dis4 fis8 gis4 h8 |
			fis4 dis8 gis4 h8 |
			b4 g8 es4 f8 |
			g4. g4 f8 |
			e4
		}
		\ottava #0 \flagFixOff
		e'8 d'4 h8 |
		a4 e8 <c d g>4\strum e8 |
		d4 r8 h, c h, |
		<dis, h, h,>4.^\fermata 8 8 8^\dc-coda \bar "||"

		\once\override Score.MetronomeMark.extra-offset = #'(-6 . -5) \tempo\markup\main-tempo "Largo." \tempo 8 = 70
		\once\override Score.RehearsalMark.extra-offset = #'(-5 . 5) \coda-ending

		\ottava #1 \arm-artif-osm \flagFixOn
		\transpose c c'
		{
			<ais, dis>8 <dis fis> <ais, dis> <dis gis> <h, h> <dis gis> |
			<ais, dis>8 <dis fis> <ais, dis> <dis gis> <h, h> cis' |
			\once\override Score.MetronomeMark.extra-offset = #'(1.5 . -3.5) \rall dis'4. 4 cis'8 |
			dis'4. 4.^\fermata \bar "|."
		}
	}

}

basses = {
	\global
	\voiceFour

	\transpose c c'
	{
		\flagOn e8\rest <g\finger "12" h\finger "12">4 e8\rest 4 \flagOff |
		a,,8 \flagOn a,\finger "12" e,\finger "12" a, e\finger "7" a\finger "7" |
		e8\rest <d'\finger "7" fis'\finger "7">4 \flagOff d,8 \flagOn d\finger "12" a,\finger "7" |
		e8\rest <g\finger "12" h\finger "12">4 c8\rest 4 |
		a,8\rest <e\finger "2+12" a\finger "2+12">4 f,8\rest <d\finger "12" g\finger "12">4 |
		d,8\rest e4\finger "2+12" d,8\rest <h,\finger "2+12" dis\finger "1+12">4 |
		s4. d,8\rest e4\finger "2+12" |
		\flagOff \stemUp <h,, dis, gis,>2. \stemDown |

		\flagOn f8\rest <fis ais>4 h8\rest <h dis'>4 |
		f8\rest <fis ais>4 h8\rest <h dis'>4 |
		d'8\rest <b d'>4 g8\rest <g c'>4 |
		d'8\rest <b d'>4 g8\rest <g h>4 |
		g8\rest <g h>4 \flagOff h,8\rest <d fis>4 |
		h,8\rest <c e>4 d,4.\strum |
	}

	\shiftOff <d c'>8 <g h> a fis e d \shiftOn |
	h,8_\fermata c h, a, g, fis, |

	%Coda

	s2. |
	s |
	h'8\rest \flag <ais' dis''>4 h'8\rest \ignore-osm <gis h h dis'>4_\markup\italic "(loco)" \unignore-osm |
	h'8\rest \flagOn ais'8 fis'' s4. \bar "|."
}

ostinato = {
	\global
	\voiceTwo

	e,4. e, |
	s2. |
	d4. s |
	<g, fis> <fis, dis> |
	a, g, |
	f, e, |
	a,8 gis, g, f,4. |
	e,2. |

	\ignore-osm
	h,4. e, |
	h,4. e, |
	es as, |
	es g, |
	c \unignore-osm h, |
	a, r8 a,4 |
	\shiftOn \once\override Dots.extra-offset = #'(.1 . .5) g,4. s \shiftOff |
	s s |

	%Coda
	\ignore-osm
	h,4. e, |
	h,4. e, |
	h,4. e, |
	h,2. \bar "|."
}

\score {
	\new Staff = "up" \with {
		\consists "Span_arpeggio_engraver"
		instrumentName = "Kytara"
	}
	{
		\set Staff.connectArpeggios = ##t
		<<
			\clef "G_8"
			\new Voice { \melody }
			\new Voice { \basses }
			\new Voice { \ostinato }
			\new Voice { \lining }
		>>
	}
	\layout { \context { \Staff \RemoveEmptyStaves \override VerticalAxisGroup.remove-first = ##t } }
}

\score {
	\new Staff \with {
		midiInstrument = "acoustic guitar (steel)"
		midiMinimumVolume = 1
		midiMaximumVolume = 1
	}
	{
		<<
			\new Voice { \clef "treble_8" \unfoldRepeats \melody }
			\new Voice { \clef "treble_8" \unfoldRepeats \basses }
			\new Voice { \clef "treble_8" \unfoldRepeats \ostinato }
		>>
	}
	\midi {
		\context {
			\Score
		}
	}
}

\paper
{
	#(set-paper-size "a5landscape")
	page-count = #1
	bookTitleMarkup = \markup\column {
		\fill-line
		{
			\fromproperty #'header:poet
			\bold\fontsize #4 \fromproperty #'header:title
			\fromproperty #'header:composer
		}
		\fill-line
		{
			\fromproperty #'header:meter
			\bold\fontsize #2 \fromproperty #'header:subtitle
			\fromproperty #'header:arranger
		}
	}
}
